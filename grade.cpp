#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>

using namespace std;

/**
*     Function that explains the usage
**/
void printUsage()
{
  printf("Usage: grade filename.java\n");
}

/**
*     Function that uses system() to run terminal commands
**/
void commands(vector<string> cmd)
{
      //compile the .java file
      system(cmd[0].c_str());
      //run the compiled java file
      system(cmd[1].c_str());
      //create new output.txt file
      system("touch output.txt");
      //make script executable and run it
      system("chmod +x script.sh");
      system("./script.sh");
}

int main(int argc, char* argv[])
{
      //check if there are enough arguments
      if (argc < 2)
      {
        printUsage();
        return 1;
      }

      //setup vector of strings to store the commands
      vector<string> cmd;

      //get the filename from the first argument
      string arg1(argv[1]);
      //create/store the compile command into the vector
      cmd.push_back("javac " + arg1);

      //remove the ".java" filetype from the filename
      string filename = arg1.substr(0, arg1.length() - 5);
      //create/store the run command into the vector
      cmd.push_back("java " + filename + " < input.txt > output.txt");

      //run the commands
      commands(cmd);

      return 0;
}
