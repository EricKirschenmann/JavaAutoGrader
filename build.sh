clear
echo "Cleaning up files..."
#remove old output
rm output.txt
rm OUTPUT
#remove old class files
rm *.class
#build the C++ project
echo "Building..."
g++ grade.cpp -o grade
echo "Usage: ./grade filename.java"
