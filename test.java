import java.text.DecimalFormat;
import java.util.Scanner;

/* CS 220 - Intro to Computer Science
 * File Name: CS220_Lab3_P2.java
 * Java Programming
 * Lab 3 - Due 10/05/2015
 * Instructor: Dr. Dan Grissom
 *
 * Name 1: Tina Bergmann
 * Name 2: Molly Reeves
 * Description: Create a program that counts change.
 */

///////////////////////////////////////////////////////////////////////////////
// INSTRUCTIONS: Update the header above EACH LAB with the correct due date,
// first/last names (remove the "Name 2" line if working alone) and description
// for this specific lab. You should also update the first/last names and problem
// number (if relevant) below in the "System.out.println" statement at the beginning
// of the "main()" method. Failure to do so will result in lost points. DO NOT change
// the name of the class or the autograder will give you 0 points.
//
// PHILOSOPHY: Lab is a chance to "get your feet wet" in a safe environment as it
// will be the first time you'll be trying to program new concepts. Thus, as you'll
// see below, the collaboration rules for LABS are very generous since the main idea
// is for you to learn programming with a lot of resources. To foster this
// environment, you'll have access to a programming partner of your choice, your peers,
// experienced lab technicians and your instructor.
//
// COLLABORATION: Students may officially work with ONE (1) partner. Both names
// should be listed above in the header and below in the "System.out.println"
// statement. When you turn your lab in, BOTH partners may (and should) turn in the
// exact same submission, although they will be graded independently. Students MAY seek
// advice and look at other students' code DURING lab (including students who are not your
// direct partner), but may NOT copy/paste code from students other than your official
// partner.
//
// If you do not finish your lab assignment during the lab period, you may only seek help
// with your code from lab technicians (available whenever the lab is open) and the instructor
// to aid with any confusion.
///////////////////////////////////////////////////////////////////////////////
public class test
{
	public static void main(String[] args)
	{
		// Your program should always output your name and the lab/problem
		// number. DO NOT DELETE OR COMMENT OUT. Replace with relevant info.
		System.out.println("Tina Bergmann & Molly Reeves");
		System.out.println("Lab 3, Problem 2");
		System.out.println("");

		// Your code should go below this line


		DecimalFormat df = new DecimalFormat("$0.00");

		final double Penny = .01;
		final double Nickle = .05;
		final double Dime = .10;
		final double Quarter = .25;

		Scanner scan = new Scanner(System.in);

		System.out.println("How many pennies do you have?");
		int numPennies = scan.nextInt();

		System.out.println("How many nickles do you have?");
		int numNickles = scan.nextInt();

		System.out.println("How many dimes do you have?");
		int numDimes = scan.nextInt();

		System.out.println("How many quarters do you have?");
		int numQuarters = scan.nextInt();

		double moneyTotal;
		moneyTotal = (numPennies * Penny + numNickles * Nickle + numDimes * Dime + numQuarters * Quarter);
		System.out.println(df.format(moneyTotal));

	}
}
